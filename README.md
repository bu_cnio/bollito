This repository has moved: https://github.com/cnio-bu/bollito

See [here](https://forum.gitlab.com/t/gitlab-introduces-user-limits-for-free-users-on-saas/64288/27)
if you'd like to know why.
